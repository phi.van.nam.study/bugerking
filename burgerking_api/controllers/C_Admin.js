class Admin{
    constructor(url){
        this.url = url;
    }

    input(
        _type='text', 
        _name='', 
        _value='',
        _id='', 
        _class='',
        _placeholder='',
        _required=true,
        _changeTitleToSlug=true
    ){
        var required = (_required==true) ? 'required' : '';
        var changeTitleToSlug = (_changeTitleToSlug==true) ? 
        'onchange="ChangeToSlug()" onkeyup="ChangeToSlug()" onkeydown="ChangeToSlug()"' : '';

        return `<input 
            type="`+_type+`" 
            name="`+_name+`" 
            value="`+_value+`"
            class="`+_class+` form-control" 
            id="`+_id+`" 
            placeholder="`+_placeholder+`" `+changeTitleToSlug+`> 
            <span class="error error_`+_name+`"></span>`; // đang tắt `+required+`
    }

    select(
        _array=[],
        _name='', 
        _id='', 
        _class='',
        _required=true,
        _dequy=false
    ){
        var required = (_required==true) ? 'required' : '';

        var str='<option value="">--Chọn--</option>';

        // xét select có đệ quy hay không
        if(_dequy==true){
            str+=this.dequy_select(_array)
        }
        else{
            _array.forEach(e=>{
                str+='<option value="'+e.value+'">'+e.name+'</option>';
            })
        }

        return `<select 
            name="`+_name+`"
            class="`+_class+` form-control"
            id="`+_id+`" `+required+`>
            `+str+`
        </select>
        <span class="error error_`+_name+`"></span>`;
    }

    textarea(
        _rows=3,
        _name='', 
        _id='', 
        _class='',
        _required=true
    ){
        var required = (_required==true) ? 'required' : '';

        return `<textarea 
            rows="`+_rows+`"
            name="`+_name+`"
            class="`+_class+` form-control"
            id="`+_id+`" `+required+`></textarea>
            <span class="error error_`+_name+`"></span>`;
    }

    changeIDtoName(key = ''){
        var str = '';

        switch(key){
            case 'name': str = 'Tên'; break;
            case 'username': str = 'Tên Đăng Nhập'; break;
            case 'password': str = 'Mật Khẩu'; break;
            case 'email': str = 'Email'; break;
            case 'phone': str = 'Số Điện Thoại'; break;
            case 'slug': str = 'Slug'; break;
            case 'parents': str = 'Cha'; break;
            default : str = 'No Name'; break;
        }
        return str;
    }

    ChangeToSlug(title) {
        var slug;
        //Đổi chữ hoa thành chữ thường
        slug = title.toLowerCase();

        //Đổi ký tự có dấu thành không dấu
        slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
        slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
        slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
        slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
        slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
        slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
        slug = slug.replace(/đ/gi, 'd');
        //Xóa các ký tự đặt biệt
        slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
        //Đổi khoảng trắng thành ký tự gạch ngang
        slug = slug.replace(/ /gi, "-");
        //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
        //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
        slug = slug.replace(/\-\-\-\-\-/gi, '-');
        slug = slug.replace(/\-\-\-\-/gi, '-');
        slug = slug.replace(/\-\-\-/gi, '-');
        slug = slug.replace(/\-\-/gi, '-');
        //Xóa các ký tự gạch ngang ở đầu và cuối
        slug = '@' + slug + '@';
        slug = slug.replace(/\@\-|\-\@|\@/gi, '');
        //In slug ra textbox có id “slug”
        return slug;
    }

    dequy(array=[], id='')
    {
        var json=[];

        array.forEach(e => {
            if(e.parents == id){ // thằng cha
                json.push({
                  name: e.name,
                  slug: e.slug,
                  childs: this.dequy(array, e._id)
                })
            }
        });

        return json;
    }
}

module.exports = Admin