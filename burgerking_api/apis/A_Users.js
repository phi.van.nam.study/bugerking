const express = require('express')
const router = express.Router()

var fs = require('fs');

// Gọi Model
const userModel = require('../models/M_User')

// Gọi bcryptjs
const bcrypt = require('bcryptjs')
const salt = bcrypt.genSaltSync(10)

// Lấy list Users
router.get('/list', (req, res) => {
    userModel
    .find({trash: false})
    .exec((error, data) => {
        if(error){
            res.send({kq: 0, Results: error})
        }
        else{
            res.send({kq: 1, Results: data})
        }
    })
})

// Tạo User
router.post('/add', (req, res) => {
    // 1. Khai báo biến
    let name, username, password, email, phone, address, flag = 1, error = '';

    // 2. Lấy giá trị
    name = req.body.name
    username = req.body.username
    password = req.body.password
    email = req.body.email
    phone = req.body.phone
    address = req.body.address
    

    // 3. Kiểm tra sữ liệu
    if(name == ''){
        flag = 0
        error = 'Không để trống tên'
    }

    // 4. Tổng kết
    if(flag == 1){
        userModel
        .find({$or: [{username}, {email}, {phone}]})
        .exec((err, data) => {
            if(err){
                res.send({kq: 0, Results: err})
            }
            else{
                if(data == ''){
                    const hash = bcrypt.hashSync(password, salt)
                    const add_obj = {
                        name,
                        username,
                        password: hash,
                        email,
                        phone,
                        address
                    }

                    userModel
                    .create(add_obj, (err, data) => {
                        if(err){
                            res.send({kq: 0, Results: err})
                        }
                        else{
                            res.send({kq: 1, Results: data})
                        }
                    })
                }
                else{
                    res.send({kq: 0, Results: 'Dữ liệu đã tồn tại'})
                }
            }
        })
    }
})

// Sửa User
router.post('/edit/:id', (req, res) => {
     // 1. Khai báo biến
     let name, username, password, email, phone, address, flag = 1, error = '';

     // 2. Lấy giá trị
     name = req.body.name
     username = req.body.username
     password = req.body.password
     email = req.body.email
     phone = req.body.phone
     address = req.body.address
 
     // 3. Kiểm tra sữ liệu

      // 4.Tổng kết
      userModel
    .find({_id: req.params.id})
    .exec((err, data) => {
        if(err){
            res.send({kq: 1, Results: 'Kết nối DB thất bại'})
        }
        else{
            var add_obj = {name, username, email, phone, address }
            userModel
            .updateOne(add_obj, (err, data) => {
                if(err){
                    res.send({kq: 0, Results: 'Kết nối DB thất bại'})
                }
                else{
                    res.send({kq: 1, Results: data})
                }
            })
        }
    })
})

// Thêm User vào trash
router.get('/add_trash/:id', (req, res) => {
    // 1. Khai báo biến
    let trash;

    // 2. Lấy giá trị
    trash = true

    // 3. Kiểm tra sữ liệu

     // 4.Tổng kết
     userModel
   .find({_id: req.params.id})
   .exec((err, data) => {
       if(err){
           res.send({kq: 1, Results: 'Kết nối DB thất bại'})
       }
       else{
           var add_obj = {trash }
           userModel
           .updateOne(add_obj, (err, data) => {
               if(err){
                   res.send({kq: 0, Results: 'Kết nối DB thất bại'})
               }
               else{
                   res.send({kq: 1, Results: 'Đã thêm vào trash'})
               }
           })
       }
   })
})

// Xóa khỏi DB
router.delete('/delete/:id', (req, res) => {
    userModel
    .find({_id: req.params.id})
    .deleteOne((err, data) => {
        if(err) {
            res.send({kq: 0, Results: 'Kết nối DB thất bại'})
        }
        else{
            res.send({kq: 1, Results: 'Đã xóa'})
        }
    })
})

router.get("/search/:key",async (req,res)=>{
    let data = await userModel.find(
        {
            "$or":[
                {name:{$regex:req.params.key}},
                {username:{$regex:req.params.key}}
            ]
        }
    )
    res.send(data);

})

module.exports = router