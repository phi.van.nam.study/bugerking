const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const multer = require('multer');

//Gọi Admin
const Admin = require('../controllers/C_Admin')

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './uploads');
  },
  filename: function(req, file, cb) {
    cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

const Product = require("../models/M_Product");

router.get("/list", (req, res) => {
  Product
  .find()
  .exec((err, data) => {
    res.send({kq: 1, Results: data})
  })
});

router.post("/add", upload.single('productImage'), (req, res) => {
  //1.Khai báo
  var name, parents, slug, price, content, error, flag = 1;

  //2.Lấy dữ liệu
  name = req.body.name;
  var use_Admin = new Admin();
  slug = use_Admin.ChangeToSlug(name);
  parents = req.body.parents;
  price = req.body.price;
  content = req.body.content;

  //3.Kiểm tra

  //4.Tổng kết
  if(flag == 1){
    Product
    .find({name})
    .exec((err, data) => {
        if(err){
            res.send({kq: 0, Results: 'Kết nối DB thất bại'})
        }
        else{
            if(data != ''){
                res.send({kq: 0, Results: 'Dữ liệu đã tồn tại'})
            }
            else{
                var add_obj = {
                  name, slug, price, content,
                  productImage: req.file.path 
                }
                //Xét parents
                if(parents != '') add_obj['parents'] = parents;

                Product
                .create(add_obj, (err, data) => {
                    if(err){
                        res.send({kq: 0, Results: 'Kết nối DB thất bại'})
                    }
                    else{
                        res.send({kq:1, Results:data})
                    }
                })
            }
        }
    })
  }
});

router.get("/:productId", (req, res, next) => {
  const id = req.params.productId;
  Product
  .findById(id)
    .exec((err, data) => {
      if(err){
        res.send({kq: 0, Results: err})
      }
      else{
        res.send({kq: 1, Results: data})
      }
    })
});

module.exports = router