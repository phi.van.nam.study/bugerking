const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    name: { type: String, require: true, unique: true },
    slug: { type: String, require: true, unique: true },
    parents: { type: mongoose.Types.ObjectId, default: null },
    price: { type: Number, default: 0 },
    content: { type: String, default: '' },
    productImage: { type: String, required: true },
    userID: { type: mongoose.Types.ObjectId, default: null },
    status: { type: Boolean, default: true },
    trash: { type: Boolean, default: false },
    date_created: { type: Date, default: Date.now() }
});

module.exports = mongoose.model('Product', productSchema);