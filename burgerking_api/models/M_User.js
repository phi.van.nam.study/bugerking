// Gọi mongoose
const mongoose = require('mongoose')

// Tạo User Schema
const userSchema = mongoose.Schema({
    name: {type: String, default: ''},
    username: {type: String, require: true, unique: true},
    password: {type: String, require: true},
    email: {type: String, require: true, unique: true},
    phone: {type: String, require: true, unique: true},
    address: {type: String, default: ''},
    role: {type: String, default: 'guest'},
    status: {type: Boolean, default: true},
    trash: {type: Boolean, default: false},
    date_created: {type: Date, default: Date.now()}
})

module.exports = mongoose.model('users', userSchema)