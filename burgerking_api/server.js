const express = require('express')
const app = express()
const dotenv = require("dotenv");
dotenv.config();

const port = process.env.PORT

// Cấu hình đường dẫn tĩnh
app.use('/api/uploads', express.static('uploads'))

// Mở port cho angular có thể truy xuất dữ liệu
app.use((req, res, next)=>{
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

// Gọi body-parser
const bodyParser = require('body-parser')

//parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}))
// parse application/json
app.use(bodyParser.json())

// Gọi kết nối DB
require('./configs/Database')

// Gọi Controls
app.use('/', require('./configs/Controls'))

app.listen(port,() =>console.log(`Server run on port ${port}`))