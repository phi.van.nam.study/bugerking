const express = require('express')
const router = express.Router()

// Gọi API
router.use('/api/users', require('../apis/A_Users'))

router.use('/api/products', require('../apis/A_Product'))

module.exports = router