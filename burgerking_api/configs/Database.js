const dotenv = require("dotenv");
dotenv.config();

// Gọi mongoose
const mongoose = require('mongoose')

// Kết nối mongoose
mongoose.connect('mongodb://localhost:27017/burgerking')
//mongoose.connect('mongodb+srv://'+process.env.DatabaseUser+':'+process.env.DatabasePass+'@nampv.hqdpcdm.mongodb.net/'+process.env.DatabaseName+'?retryWrites=true&w=majority')
// mongoose.connect('mongodb+srv://NamPV:Admin@nampv.hqdpcdm.mongodb.net/Burgerking?retryWrites=true&w=majority')
.then(() => console.log('Kết nối DB thành công !'))
.catch(() => console.log('Kết nối DB thất bại !'))